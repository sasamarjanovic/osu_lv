import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.model_selection import train_test_split
import sklearn.linear_model as lm
from sklearn.preprocessing import OneHotEncoder

data = pd.read_csv('data_C02_emission.csv')

data = data.drop(['Make', 'Model'], axis=1)
input = ['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Hwy (L/100km)', 'Fuel Type']
output = ['CO2 Emissions (g/km)']

ohe = OneHotEncoder ()
X_encoded = ohe.fit_transform(data[['Fuel Type']]).toarray()
data['Fuel Type'] = X_encoded

X = data[input].to_numpy()
y = data[output].to_numpy()

X_train , X_test , y_train , y_test = train_test_split (X , y , test_size = 0.2 , random_state =1 )

linearModel = lm.LinearRegression()
linearModel.fit( X_train, y_train )

print(linearModel.intercept_, linearModel.coef_)

y_test_p = linearModel.predict(X_test)

plt.figure()
for i in range(6):
    plt.scatter(X_test[:, i], y_test, c='blue', s=1)
    plt.scatter(X_test[:, i], y_test_p, c='red', s=1)
    plt.xlabel(input[i])
    plt.ylabel('CO2 Emissions (g/km)')
    plt.legend(('Real output', 'Predicted output'))
    plt.show()


error = abs(y_test_p - y_test)
print(np.max(error))

max_error_id = np.argmax(error)
data = pd.read_csv('data_C02_emission.csv')
max_error_model = data.iloc[max_error_id, 1]
print(max_error_model)