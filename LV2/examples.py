import numpy as np
import matplotlib.pyplot as plt

print('2.1')

a = np.array([6, 2, 9]) 
print(type(a)) 
print(a.shape) 
print(a[0], a[1], a[2]) 
a[1] = 5
print(a)

print(a[1:2]) 
print(a[1:-1]) 
b = np.array([[3,7,1],[4,5,6]])  
print(b.shape) 
print(b.ndim)
print(b) 
print(b[0, 2], b[0, 1], b[1, 1]) 
print(b[0:2,0:1]) 
print(b[:,0]) 

c = np.zeros((4,2))
d = np.ones((3,2))
e = np.full((1,2),5)
f = np.eye(2) 
g = np.array([1, 2, 3], np.float32) 
duljina = len(g)
print(duljina)
h = g.tolist()
print(h)
c = g.transpose() 
print(g) 
print(np.concatenate((a, g,)))


print('\n2.2')

a = np.array([3,1,5], float) 
b = np.array([2,4,8], float) 
print(a+b)
print(a-b)
print(a*b) 
print(a/b)
print(a.min())

print(a.argmin()) 
print(a.max()) 
print(a.argmax()) 
print(a.sum()) 
print(a.mean())

print(np.mean(a)) 
print(np.max(a)) 
print(np.sum(a))
a.sort() 
print(a)


print('\n2.3')

np.random.seed(56)
rNumbers = np.random.rand(10) 
print(rNumbers) 
print(rNumbers.mean())


print('\n2.4')

x = np.linspace(0, 6, num=30)
y= np.sin(x)
plt.plot(x, y, 'b', linewidth=1, marker=".", markersize=5) 
plt.axis([0,6,-2,2])
plt.xlabel('x')
plt.ylabel('vrijednosti funkcije')
plt.title('sinus funkcija') 
plt.show()


print('\n2.5')

img = plt.imread("road.jpg")
img = img[:,:,0].copy() 
print(img.shape) 
print(img.dtype)
plt.figure()
plt.imshow(img, cmap="gray")
plt.show()