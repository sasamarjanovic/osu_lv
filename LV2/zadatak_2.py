import numpy as np
import matplotlib.pyplot as plt 

data = np.loadtxt('data.csv', delimiter=',', skiprows=1)
print(data)
rows, cols = data.shape
print('people', rows)


hight = data[:, 1]
weight = data[:, 2]
plt.scatter(weight, hight, s=0.1)
plt.axis ([25,130, 130,205])
plt.ylabel ('hight')
plt.xlabel (' weight ')
plt.show ()


print('min hight', hight.min())
print('max hight', hight.max())
print('mean hight', hight.mean())


hight = data[0::50, 1]
weight = data[0::50, 2]
plt.scatter(weight, hight)
plt.axis ([25,130, 130,205])
plt.ylabel ('hight')
plt.xlabel (' weight ')
plt.show ()


m = (data[:, 0] == 1)
z = (data[:, 0] == 0)

print('man min hight', data[m,1].min())
print('man max hight', data[m,1].max())
print('man mean hight', data[m,1].mean())

print('woman min hight', data[z,1].min())
print('woman max hight', data[z,1].max())
print('woman mean hight', data[z,1].mean())