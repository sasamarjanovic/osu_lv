import pandas as pd
import numpy as np

data = pd.read_csv('data_C02_emission.csv')
print('a)')
print('Broj mjerenja', len(data))
print(data.info())
print('Broj null vrijednosti po stupcu', data.isnull().sum())
print('Broj duplikata', data.duplicated().sum())
data.dropna(axis=0)
data.drop_duplicates()
data = data.reset_index()
for col in data.select_dtypes(include='object'):
    data[col] = data[col].astype('category')
print(data.info())


print('\nb)')
print(data.sort_values(by='Fuel Consumption City (L/100km)', ascending=True)[['Make', 'Model', 'Fuel Consumption City (L/100km)']].head(3))
print(data.sort_values(by='Fuel Consumption City (L/100km)', ascending=False)[['Make', 'Model', 'Fuel Consumption City (L/100km)']].head(3))


print('\nc)')
print(len(data[(data['Engine Size (L)'] > 2.5) & (data['Engine Size (L)'] < 3.5)]))
print(data[(data['Engine Size (L)'] > 2.5) & (data['Engine Size (L)'] < 3.5)]['CO2 Emissions (g/km)'].mean())


print('\nd)')
print(len(data[data.Make == 'Audi']))
print(data[(data.Make == 'Audi') & (data.Cylinders == 4)]['CO2 Emissions (g/km)'].mean())


print('\ne)')
print(data.groupby(by='Cylinders')['Cylinders'].count())
print(data.groupby(by='Cylinders')['CO2 Emissions (g/km)'].mean())


print('\nf))')
print(data[data['Fuel Type'] == 'D']['Fuel Consumption City (L/100km)'].mean())
print(data[data['Fuel Type'] == 'X']['Fuel Consumption City (L/100km)'].mean())

print('\ng)')
print(data[(data['Fuel Type'] == 'D') & (data.Cylinders == 4)].sort_values(by='Fuel Consumption City (L/100km)', ascending=False)[['Make', 'Model', 'Fuel Type', 'Fuel Consumption City (L/100km)']].head(1))

print('\nh)')
print(len(data[data.Transmission.str.startswith('M')]))

print('\ni)')
print(data.corr(numeric_only=True))

