flag = True
numbers = []
while flag:
    number = input('Enter number or "Done":')
    if number == 'Done':
        flag = False
        break
    try:
        number = float(number)
    except ValueError: 
        print('Sorry, you did not enter number')
    numbers.append(number)

print(len(numbers))
print(min(numbers))
print(max(numbers))
print(sum(numbers)/len(numbers))
numbers.sort()
print(numbers)