try:
    input = float(input('Enter number [0.0, 1.0]: '))
    if input < 0 or input > 1:
        raise Exception("Sorry, number is not in interval")
    if(input >= 0.9):
        print('A')
    elif(input >= 0.8):
        print('B')
    elif(input >= 0.7):
         print('C')
    elif(input >= 0.6):
         print('D')
    else:
        print('F')
except ValueError:
    print('Sorry, you did not enter number')
except Exception as e:
    print(str(e))